# coding: utf-8
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from bs4.element import Tag
from codecs import getwriter
import json
import requests
from requests.adapters import HTTPAdapter
from signal import signal, SIGINT, SIGTERM
from sys import stdout, exit, exc_info, stderr
import threading
import Queue

TIMEOUT = 90
MAX_RETRIES = 20
ALIVE = True
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}


def clean(val):
    return val.strip()


class BaseParser(object):
    def __init__(self, page_url, **kwargs):
        self._session = requests.Session()
        self._session.headers.update(headers)
        self._session.mount(page_url, HTTPAdapter(max_retries=MAX_RETRIES))
        self._url = page_url
        self._BASE = kwargs

    def set_value(self, recipe, method, html):
        key, val = self.call_method(method, html)
        if val:
            recipe[key] = val
        return recipe

    def call_method(self, method, html):
        try:
            return getattr(self, method)(html)
        except:
            return (None, None, )


class CategoryParser(BaseParser):
    THREADS = 4
    
    def get_result(self):
        response = self._session.get(self._url)
        soup = BeautifulSoup(response.content, 'html.parser')
        try:
            page_number = int(soup.find('div', attrs={'class':'post-pagination clearfix'}).findAll('a')[-2].text)
        except:
            page_number = 1
        work_q = Queue.Queue()
        work_recipe = Queue.Queue()
        work_q.put(self._url)
        for i in range(2, page_number+1):
            work_q.put(self._url+'/page/'+str(i))

        def mult_process(work_q, work_recipe):
            while ALIVE:
                if work_q.empty():
                    break
                page_url = work_q.get()
                response = self._session.get(page_url)
                soup = BeautifulSoup(response.content, 'html.parser')
                for article in soup.findAll('article'):
                    a = article.a
                    work_recipe.put(a.get('href'))

        threads = []
        thread_count = self.THREADS if self.THREADS < work_q.qsize() else work_q.qsize()
        for x in range(thread_count):
            threads.append(threading.Thread(target=mult_process, args=(work_q, work_recipe)))
        for t in threads:
            t.start()
        for t in threads:
            t.join()

        result = []
        while not work_recipe.empty():
            result.append(work_recipe.get_nowait())
        return result


class RecipeParser(BaseParser):
    fields = ('name', 'image', 'tags', 'type', 'description', 'cooking_time', 'preparation_time', 'ingredients', )

    def get_name(self, html):
        return 'name', html.find('h1', attrs={'itemprop': 'name'}).text

    def get_image(self, html):
        return 'images', [html.find('img', attrs={'itemprop': 'image'}).get('src')]

    def get_tags(self, html):
        result = []
        result.append(clean(html.find('div', attrs={'class': 'kama_breadcrumbs'}).findAll('a')[1].text))
        return 'tags', result

    def get_type(self, html):
        result = []
        result.append(clean(html.find('div', attrs={'class': 'kama_breadcrumbs'}).findAll('a')[2].text))
        return 'type', result

    def get_description(self, html):
        result = ''
        for child in html.find('div', attrs={'class': 'entry clearfix'}).contents:
            if not isinstance(child, Tag):
                continue
            if child.name != 'p':
                break
            result += ' '+ clean(child.text)
        return 'description', clean(result)

    def get_cooking_time(self, html):
        return 'cooking_time', clean(html.find('time', attrs={'itemprop': 'cookTime'}).text)

    def get_preparation_time(self, html):
        return 'preparation_time', clean(html.find('time', attrs={'itemprop': 'prepTime'}).text)

    def get_ingredients(self, html):
        ingredients = []
        for li in html.findAll('li', attrs={'itemprop': 'ingredients'}):
            val = clean(li.text)
            if val[-1] in {';', '.'}:
                val = val[:-1]
            data = {
                'full': val,
            }
            if val.find(u'–') > -1:
                quantity = val.split(u'–')[-1]
                name = u'–'.join(val.split(u'–')[:-1])
                data['name'] = [clean(name)]
                data['quantity'] = clean(quantity)
            else:
                data['name'] = [val]
            ingredients.append(data)
        return 'ingredients', ingredients

    def get_recipe_instructions(self, html):
        step_by_step_recipe = []
        for li in html.findAll('li'):
            data = {'description': clean(li.text)}
            if li.find('img'):
                data['images'] = [clean(x.get('src')) for x in li.findAll('img')]
            step_by_step_recipe.append(data)
        return 'step_by_step_recipe', step_by_step_recipe

    def get_alter_recipe_instructions(self, html):
        step_by_step_recipe = []
        current_step = {'images': [], 'description': ''}
        begin_steps = False
        for child in html.find('div', attrs={'class': 'entry clearfix'}).contents:
            if not isinstance(child, Tag):
                continue
            if child.get('id') == 'yandex_ad3':
                begin_steps = True
                continue
            if not begin_steps or child.name != 'p':
                continue
            current_step['description'] = clean(child.text)
            if child.find('img'):
                if step_by_step_recipe:
                    current_step['images'].append(child.find('img').get('src'))
            else:
                if current_step['description']:
                    if not current_step['images']:
                        del current_step['images']
                    step_by_step_recipe.append(current_step)
                current_step = {'images': [], 'description': ''}
        else:
            if current_step['description']:
                if not current_step['images']:
                    del current_step['images']
                step_by_step_recipe.append(current_step)
        return 'step_by_step_recipe', step_by_step_recipe

    def get_other_recipe_instructions(self, html):
        step_by_step_recipe = []

        def add_to_step(step, steps):
            if step.get('description'):
                if 'images' in step and not step['images']:
                    del step['images']
                steps.append(step)
            return {'images': [], 'description': ''}

        current_step = add_to_step({}, step_by_step_recipe)
        for child in html.contents:
            if not isinstance(child, Tag):
                continue
            if child.name == 'h3':
                current_step = add_to_step(current_step, step_by_step_recipe)
                current_step['description'] = clean(child.text)
            elif child.name == 'p':
                if child.findAll('img'):
                    for img in child.findAll('img'):
                        current_step['images'].append(clean(img.get('src')))
                else:
                    current_step['description'] += u' %s' % clean(child.text)
        else:
            if current_step['description']:
                if not current_step['images']:
                    del current_step['images']
                step_by_step_recipe.append(current_step)
        return 'step_by_step_recipe', step_by_step_recipe

    def get_result(self):
        recipe = {
            'url': clean(self._url),
        }
        response = self._session.get(self._url)
        soup = BeautifulSoup(response.content, 'html.parser')
        content = soup.find('section', attrs={'id': 'content'}).article
        for field_name in self.fields:
            recipe = self.set_value(recipe, 'get_%s' % field_name, content)

        no_photos = content.find('ol', attrs={'itemprop': 'recipeInstructions'})
        if no_photos:
            recipe = self.set_value(recipe, 'get_recipe_instructions', no_photos)
        else:
            if content.find('div', attrs={'id': 'instr'}):
                recipe = self.set_value(recipe, 'get_other_recipe_instructions', content.find('div', attrs={'id': 'instr'}))
            else:
                recipe = self.set_value(recipe, 'get_alter_recipe_instructions', content)
        return recipe


class Client(object):
    THREADS = 32

    def __init__(self, sout, BASE):
        self._BASE = BASE
        self.sout = sout
        self._session = requests.Session()
        self._session.headers.update(headers)
        self._session.mount(self._BASE['url'], HTTPAdapter(max_retries=MAX_RETRIES))

        self._queue = []
        # пример получения страницы
        response = self._session.get(self._BASE['url'])
        soup = BeautifulSoup(response.content, 'html.parser')
        for li in next(soup.find('ul',attrs={'id':'mainnav-menu'}).children).ul.children:
            a = li.find('a')
            if a == -1:
                continue
            self._queue.append(a.get('href'))
        self._queue += self._BASE['cats']
        
    def _run_first(self):
        work_q = Queue.Queue()
        work_recipe = Queue.Queue()
        for page_url in self._queue:
            # заталкиваем в очередь
            work_q.put(page_url)

        def mult_process(work_q, work_recipe):
            while ALIVE:
                if work_q.empty():
                    break
                page_url = work_q.get()
                try:
                    for val in CategoryParser(page_url, **self._BASE).get_result():
                        work_recipe.put(val)
                except Exception as e:
                    pass

        threads = []
        thread_count = self.THREADS if self.THREADS < len(self._queue) else len(self._queue)
        for x in range(thread_count):
            threads.append(threading.Thread(target=mult_process, args=(work_q, work_recipe)))
        for t in threads:
            t.start()
        for t in threads:
            t.join()
        return work_recipe
        
    def _run_second(self, work_q):
        def mult_process(work_q):
            while ALIVE:
                if work_q.empty():
                    break
                page_url = work_q.get()
                try:
                    recipe = RecipeParser(page_url, **self._BASE).get_result()
                    self.sout.write(json.dumps(recipe, ensure_ascii=False) + "\n")
                except Exception as e:
                    pass

        threads = []
        thread_count = self.THREADS if self.THREADS < len(self._queue) else len(self._queue)
        for x in range(thread_count):
            threads.append(threading.Thread(target=mult_process, args=(work_q, )))
        for t in threads:
            t.start()
        for t in threads:
            t.join()

    def process(self):
        work_q = self._run_first()
        self._run_second(work_q)


# Основной код
def main():
    sout = getwriter("utf8")(stdout) 
    serr = getwriter("utf8")(stderr)
    # Указываем целевой url и имя
    URLS = {
        'chudo-povar': {
            'url': u'http://chudo-povar.com/',
            'cats': [
                u'http://chudo-povar.com/recepty/zagotovki-na-zimu',
                u'http://chudo-povar.com/recepty/recepty-dlya-mikrovolnovki',
                u'http://chudo-povar.com/recepty/recepty-dlya-multivarki',
                u'http://chudo-povar.com/recepty/postnye-blyuda',
            ]
        },
    }
    client = Client(sout, URLS['chudo-povar'])
    
    try:
        client.process()
    except Exception as e:
        exc_traceback = exc_info()[2]
        filename = line = None
        while exc_traceback is not None:
            f = exc_traceback.tb_frame
            line = exc_traceback.tb_lineno
            filename = f.f_code.co_filename
            exc_traceback = exc_traceback.tb_next
        serr.write(json.dumps({
            'error': True,
            'details': {
                'message': str(e),
                'file': filename,
                'line': line
            }
        }, ensure_ascii=False) + "\n")


if __name__ == "__main__": 
    def signal_handler(signal, frame):
        global ALIVE
        ALIVE = False
        exit(0)
    signal(SIGINT, signal_handler)
    signal(SIGTERM, signal_handler)
    main() 
